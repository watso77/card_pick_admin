<?php 
class StringHelper{
	
	public static function toAsterisk($str)	{
		$toA = "";
		for($i = 0; $i < strlen($str); $i++){
			$toA .= "*";
		}
		return $toA;
	}
	
	public static function getSafeCallResult($code){
		//인포필러
		if ($code ==  "00") return "성공";
		if ($code ==  "01") return "패킷 길이 에러";
		if ($code ==  "07") return "전화번호 형식이 맞지 않음";
		if ($code ==  "12") return "지정되지 않은 050 번호";
		if ($code ==  "21") return "인증에러";
		if ($code ==  "22") return "전송시간 초과";
		if ($code ==  "23") return "시스템 에러";
		if ($code ==  "24") return "기타 오류";
		return "Unkown fial";
	}
}

class ListHelper{

	/*Total record*/
	public function getQueryTotalCount($mysqliObj, $sql){
	
		if ($stmtTotal = $mysqliObj->prepare($sql)) {
			$stmtTotal->execute();
			$stmtTotal->store_result();
			$totalCount = $stmtTotal->affected_rows;
			$stmtTotal->close();
			return  $totalCount;
		}
		return 0;
	}
	
	/*@param
	 * $mode : "start", "end"
	 * */
	public function calPage($currentPage, $totalCount, $mode){
		$totalPage = $totalCount / SAFE_CALL_LIST_PER_PAGE;
		$remainListCount = $totalCount % SAFE_CALL_LIST_PER_PAGE;
		if ($remainListCount > 0) $totalPage++;
		$cal0 = floor($currentPage /  10);
	
		if ($cal0 == 0){
			$mode == "start" ? $result = 1 : $result =  1 + 9;
			return $result;
	
		}else{
			$mode == "start" ? $result = $cal0 * 10 : $result = $cal0 * 10 + 9;
			return $result;
		}
	}
	
	public function getTotalPageCount($totalCount){
		$totalPage = $totalCount / SAFE_CALL_LIST_PER_PAGE;
		$remainListCount = $totalCount % SAFE_CALL_LIST_PER_PAGE;
		if ($remainListCount > 0) $totalPage++;
		return $totalPage;
	}
	
	public function getStmt($mysqliObj, $sql){
		if($stmt = $mysqliObj->prepare($sql)){
			$stmt->execute();
			$stmt->store_result();
			return $stmt;
		}
		return null;
	}
}

class Paging{
	
	public function printSafeCallPaging($stmt, $page , $totalCount, $perpage, $urlParams){
		//$totalCount = $stmt->affected_rows;
		$pageTotal  = ceil($totalCount / $perpage);
		$isFirst = $page == 1;
		$isLast  = $page == $pageTotal;
		$prev = max(1, $page - 1);
		$next = min($pageTotal , $page + 1);
	
		echo
		"<div align='center' style='margin:20px'>";
	
		$SELF_PREV = $_SERVER['PHP_SELF'] . "?page=" . $prev . $urlParams;
		$SELF_FIRST = $_SERVER['PHP_SELF'] . "?page=1" . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_FIRST' style='margin-right:20px'>&nbsp;&nbsp;처음&nbsp;&nbsp;</a>";
		echo "<a class='myPageNum' href='$SELF_PREV'> 이전 </a>";
	
		for($i = (new ListHelper)->calPage($page, $totalCount, "start"); 
			$i <= (new ListHelper)->calPage($page, $totalCount, "end"); 
			$i++)
		{
		
			if ($i > (new ListHelper)->getTotalPageCount($totalCount) ) break;
				
			$SELF = $_SERVER['PHP_SELF'] . "?page=" . $i . $urlParams;
			if ($page == $i)
				echo "<a class='myPageNum' href='$SELF'><b>" . $i ."</b></a>";
			else
				echo "<a class='myPageNum' href='$SELF'>" . $i ."</a>";
		}
	
		$SELF_NEXT = $_SERVER['PHP_SELF'] . "?page=" . $next . $urlParams;
		$SELF_LAST = $_SERVER['PHP_SELF'] . "?page=" . $pageTotal . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_NEXT'> 다음 </a>";
		echo "<a class='myPageNum' href='$SELF_LAST' style='margin-left:20px'>마지막</a>";
		echo
		"</div>";
	}
	
	public function printMemberPaging($stmt, $page , $totalCount, $perpage, $urlParams){
		//$totalCount = $stmt->affected_rows;
		$pageTotal  = ceil($totalCount / $perpage);
		$isFirst = $page == 1;
		$isLast  = $page == $pageTotal;
		$prev = max(1, $page - 1);
		$next = min($pageTotal , $page + 1);
	
		echo
		"<div align='center' style='margin:20px'>";
	
		$SELF_PREV = $_SERVER['PHP_SELF'] . "?page=" . $prev . $urlParams;
		$SELF_FIRST = $_SERVER['PHP_SELF'] . "?page=1" . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_FIRST' style='margin-right:20px'>&nbsp;&nbsp;처음&nbsp;&nbsp;</a>";
		echo "<a class='myPageNum' href='$SELF_PREV'> 이전 </a>";
	
		for($i = (new ListHelper)->calPage($page, $totalCount, "start");
		$i <= (new ListHelper)->calPage($page, $totalCount, "end");
		$i++)
		{
	
			if ($i > (new ListHelper)->getTotalPageCount($totalCount) ) break;
	
			$SELF = $_SERVER['PHP_SELF'] . "?page=" . $i . $urlParams;
			if ($page == $i)
				echo "<a class='myPageNum' href='$SELF'><b>" . $i ."</b></a>";
				else
					echo "<a class='myPageNum' href='$SELF'>" . $i ."</a>";
		}
	
		$SELF_NEXT = $_SERVER['PHP_SELF'] . "?page=" . $next . $urlParams;
		$SELF_LAST = $_SERVER['PHP_SELF'] . "?page=" . $pageTotal . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_NEXT'> 다음 </a>";
		echo "<a class='myPageNum' href='$SELF_LAST' style='margin-left:20px'>마지막</a>";
		echo
		"</div>";
	}
	
	public function printAppVersionPaging($stmt, $page , $totalCount, $perpage, $urlParams){
		//$totalCount = $stmt->affected_rows;
		$pageTotal  = ceil($totalCount / $perpage);
		$isFirst = $page == 1;
		$isLast  = $page == $pageTotal;
		$prev = max(1, $page - 1);
		$next = min($pageTotal , $page + 1);
	
		echo
		"<div align='center' style='margin:20px'>";
	
		$SELF_PREV = $_SERVER['PHP_SELF'] . "?page=" . $prev . $urlParams;
		$SELF_FIRST = $_SERVER['PHP_SELF'] . "?page=1" . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_FIRST' style='margin-right:20px'>&nbsp;&nbsp;처음&nbsp;&nbsp;</a>";
		echo "<a class='myPageNum' href='$SELF_PREV'> 이전 </a>";
	
		for($i = (new ListHelper)->calPage($page, $totalCount, "start");
		$i <= (new ListHelper)->calPage($page, $totalCount, "end");
		$i++)
		{
	
			if ($i > (new ListHelper)->getTotalPageCount($totalCount) ) break;
	
			$SELF = $_SERVER['PHP_SELF'] . "?page=" . $i . $urlParams;
			if ($page == $i)
				echo "<a class='myPageNum' href='$SELF'><b>" . $i ."</b></a>";
				else
					echo "<a class='myPageNum' href='$SELF'>" . $i ."</a>";
		}
	
		$SELF_NEXT = $_SERVER['PHP_SELF'] . "?page=" . $next . $urlParams;
		$SELF_LAST = $_SERVER['PHP_SELF'] . "?page=" . $pageTotal . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_NEXT'> 다음 </a>";
		echo "<a class='myPageNum' href='$SELF_LAST' style='margin-left:20px'>마지막</a>";
		echo
		"</div>";
	}
	
	public function printHistorySafeCallPaging($stmt, $page , $totalCount, $perpage, $urlParams){
		//$totalCount = $stmt->affected_rows;
		$pageTotal  = ceil($totalCount / $perpage);
		$isFirst = $page == 1;
		$isLast  = $page == $pageTotal;
		$prev = max(1, $page - 1);
		$next = min($pageTotal , $page + 1);
	
		echo
		"<div align='center' style='margin:20px'>";
	
		$SELF_PREV = $_SERVER['PHP_SELF'] . "?page=" . $prev . $urlParams;
		$SELF_FIRST = $_SERVER['PHP_SELF'] . "?page=1" . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_FIRST' style='margin-right:20px'>&nbsp;&nbsp;처음&nbsp;&nbsp;</a>";
		echo "<a class='myPageNum' href='$SELF_PREV'> 이전 </a>";
	
		for($i = (new ListHelper)->calPage($page, $totalCount, "start");
		$i <= (new ListHelper)->calPage($page, $totalCount, "end");
		$i++)
		{
	
			if ($i > (new ListHelper)->getTotalPageCount($totalCount) ) break;
	
			$SELF = $_SERVER['PHP_SELF'] . "?page=" . $i . $urlParams;
			if ($page == $i)
				echo "<a class='myPageNum' href='$SELF'><b>" . $i ."</b></a>";
				else
					echo "<a class='myPageNum' href='$SELF'>" . $i ."</a>";
		}
	
		$SELF_NEXT = $_SERVER['PHP_SELF'] . "?page=" . $next . $urlParams;
		$SELF_LAST = $_SERVER['PHP_SELF'] . "?page=" . $pageTotal . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_NEXT'> 다음 </a>";
		echo "<a class='myPageNum' href='$SELF_LAST' style='margin-left:20px'>마지막</a>";
		echo
		"</div>";
	}
	
	public function printPlannerPaging($stmt, $page , $totalCount, $perpage, $urlParams){
		//$totalCount = $stmt->affected_rows;
		$pageTotal  = ceil($totalCount / $perpage);
		$isFirst = $page == 1;
		$isLast  = $page == $pageTotal;
		$prev = max(1, $page - 1);
		$next = min($pageTotal , $page + 1);
	
		echo
		"<div align='center' style='margin:20px'>";
	
		$SELF_PREV = $_SERVER['PHP_SELF'] . "?page=" . $prev . $urlParams;
		$SELF_FIRST = $_SERVER['PHP_SELF'] . "?page=1" . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_FIRST' style='margin-right:20px'>&nbsp;&nbsp;처음&nbsp;&nbsp;</a>";
		echo "<a class='myPageNum' href='$SELF_PREV'> 이전 </a>";
	
		for($i = (new ListHelper)->calPage($page, $totalCount, "start");
		$i <= (new ListHelper)->calPage($page, $totalCount, "end");
		$i++)
		{
	
			if ($i > (new ListHelper)->getTotalPageCount($totalCount) ) break;
	
			$SELF = $_SERVER['PHP_SELF'] . "?page=" . $i . $urlParams;
			if ($page == $i)
				echo "<a class='myPageNum' href='$SELF'><b>" . $i ."</b></a>";
				else
					echo "<a class='myPageNum' href='$SELF'>" . $i ."</a>";
		}
	
		$SELF_NEXT = $_SERVER['PHP_SELF'] . "?page=" . $next . $urlParams;
		$SELF_LAST = $_SERVER['PHP_SELF'] . "?page=" . $pageTotal . $urlParams;
	
		echo "<a class='myPageNum' href='$SELF_NEXT'> 다음 </a>";
		echo "<a class='myPageNum' href='$SELF_LAST' style='margin-left:20px'>마지막</a>";
		echo
		"</div>";
	}
}

?>
		




