<?php
header('Content-Type: text/html; charset=utf-8');
include "./config/define.php";
include "./config/db.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$sql = "INSERT INTO planner(name
			, email
			, tel
			, addr
			, major_card_company
			, planner_num
			, issued_zone
			, register_id
			, gender
			, status
			, password
			, date_insert)
			 VALUES (?
					, ?
					, ?
					, ?
					, ?
					, ?
					, ?
					, ?
					, ?
					, ?
					, ?
					, now())";
	
echo $sql;
$stmt = $mysqli->prepare($sql);
$stmt->bind_param('sssssssssis'
		, $name
		, $email
		, $tel
		, $addr
		, $major_card_company
		, $planner_num
		, $issued_zone
		, $register_id
		, $gender
		, $status
		, $password
		);

$name = $_GET['name'];
$email = $_GET['email'];
$tel = $_GET['tel'];
$addr= $_GET['addr'];
$major_card_company = $_GET['major_card_company'];
$planner_num= $_GET['planner_num'];
$issued_zone= $_GET['issued_zone'];
$register_id= $_GET['register_id'];
$gender= $_GET['gender'];
$status= $_GET['status'];
$password= $_GET['password'];

$stmt->execute();
$stmt->close();
$mysqli->close();

echo "
<script>
	alert('등록 완료');
		history.back();
		location.href='./planner_list.php';
</script>
";




?>