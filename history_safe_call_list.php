<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title
안심콜 전화내역
</title>

<?php 
include 'meta.php';
?>
</head>

<body>

<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'head_navi.php';
include "./config/define.php";
include "./config/db.php";
include './lib/util.php';
?>

<!-- Your Content -->
<div id="container">
	<h2>안심콜 전화내역</h2>

	<?php 
		$listHelper= new ListHelper;
		
		$urlParams = "";
		$totalCount = $listHelper->getQueryTotalCount($mysqli, "SELECT * FROM call_history");
		$perpage = SAFE_CALL_LIST_PER_PAGE; // This you can define yourself
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$startIndex = ((int)$page * (int)$perpage) - $perpage;
		$limitStr = "LIMIT " . $startIndex . "," . $perpage;
		
		//where 절 추가 요망
		$sql = "SELECT
				id
				, registerd_id
				, VirtualNumber
				, Result
				, date
				, PhoneNumber
				, TotalCount
				, UseCount
				, ETCValue
				, SYSTEMID
				, ActionType
				, planner_tel
				FROM call_history
				ORDER BY id ASC
				$limitStr";
				
		$stmt = $listHelper->getStmt($mysqli, $sql);
		$stmt->bind_result(
				$id
				, $registerd_id
				, $VirtualNumber
				, $Result
				, $date
				, $PhoneNumber
				, $TotalCount
				, $UseCount
				, $ETCValue
				, $SYSTEMID
				, $ActionType
				, $planner_tel
				);
		
		if ($totalCount == 0)
			echo"등록 내용이 없습니다.<div class='CSSTableGenerator' style='display:none' >";
		else
			echo"<div class='CSSTableGenerator' >";
		
		echo 
		"<table border='1' width='100%' >
			<tr>
				<td>번호</td>
				<td>회원ID</td>
				<td>VirtualNumber</td>
				<td>Result</td>
				<td>일시</td>
				<td>회원전화</td>
				<td>안심콜총개수</td>
				<td>남은개수</td>
				<td>ETCValue</td>
				<td>SYSTEMID</td>
				<td>ActionType</td>
				<td>설계사전화</td>
				<td>설계사ID</td>
			</tr>
		";
		
		$num  =  $totalCount - ($page * $perpage) + $perpage;
		while ($stmt->fetch()) {
			$result = StringHelper::getSafeCallResult($Result);
			
			echo
			"<tr>
				<td>$num</td>
				<td>$registerd_id</td>
				<td>$VirtualNumber</td>
				<td>$result</td>
				<td>$date</td>
				<td>$PhoneNumber</td>
				<td>$TotalCount</td>
				<td>$UseCount</td>
				<td>$ETCValue</td>
				<td>$SYSTEMID</td>
				<td>$ActionType</td>
				<td>$planner_tel</td>
				<td>추가사항</td>
			</tr>
			";
			$num--;
		}
		
		$stmt->close();
		$mysqli->close();
		
		echo 
		"<table>";
		
		(new Paging)->printHistorySafeCallPaging($stmt, $page, $totalCount, $perpage, $urlParams);
		
	?>
	</div>

</div>


<?php 
include 'footer.php';
?>

</body>
</html>