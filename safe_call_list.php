<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>
	안심콜 리스트
</title>

<?php 
include 'meta.php';
?>
</head>

<body>

<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'head_navi.php';
include "./config/define.php";
include "./config/db.php";
include './lib/util.php';
?>

<!-- Your Content -->
<div id="container">
	

	<?php 
		$listHelper= new ListHelper;
	
		//set
		$urlParams = "";
		$totalCount = (new ListHelper)->getQueryTotalCount($mysqli, "select * from call_grant");
		$perpage = SAFE_CALL_LIST_PER_PAGE; // This you can define yourself
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$startIndex = ((int)$page * (int)$perpage) - $perpage;
		$limitStr = "LIMIT " . $startIndex . "," . $perpage;
		
		echo "<h2>안심콜 리스트 (총:$totalCount 개)</h2> ";
		
		//where 절 추가 요망
		$sql = "SELECT
				id
				, VirtualNumber
				, PhoneNumber
				, member_register_id
				, consumer_company
				, type
				, ActionType
				, date_input
				FROM call_grant
				ORDER BY id ASC
				$limitStr";
				
		$stmt = $listHelper->getStmt($mysqli, $sql);
		$stmt->bind_result(
				$id
				, $VirtualNumber
				, $PhoneNumber
				, $member_register_id
				, $consumer_company
				, $type
				, $ActionType
				, $date_input
			);
		
		if ($totalCount == 0)
			echo"등록 내용이 없습니다.<div class='CSSTableGenerator' style='display:none' >";
		else
			echo"<div class='CSSTableGenerator' >";
		
		echo 
		"<table border='1' width='100%' >
			<tr>
				<td>번호</td>
				<td>안심번호</td>
				<td>회원전화번호</td>
				<td>회원아이디</td>
				<td>타입</td>
				<td>업체</td>		
				<td>부여일시</td>		
			</tr>
		";
		
		$num  =  $totalCount - ($page * $perpage) + $perpage;
		
		while ($stmt->fetch()) {
			if($ActionType == "1")	$at = "등록";
			else if($ActionType == "2")	$at = "해지";
			else if($ActionType == "3")	$at = "발급";
			
			echo
			"<tr>
			<td>$num</td>
			<td>$VirtualNumber</td>
			<td>$PhoneNumber</td>
			<td>$member_register_id</td>
			<td>$type</td>
			<td>$consumer_company</td>
			<td>$date_input</td>
			</tr>
			";
			$num--;
		}
		
		$stmt->close();
		$mysqli->close();
		
		echo 
		"<table>";
		
		(new Paging)->printSafeCallPaging($stmt, $page, $totalCount, $perpage, $urlParams);
		
	?>
	</div>

</div>

<?php 
include 'footer.php';
?>


</body>
</html>