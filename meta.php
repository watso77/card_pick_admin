<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Pushy - Off-Canvas Navigation Menu</title>
<meta name="description" content="Pushy is an off-canvas navigation menu for your website.">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/demo.css">
<link rel="stylesheet" href="css/table.css">
<link rel="stylesheet" href="css/etc.css">
<link rel="stylesheet" href="css/button.css">
<!-- Pushy CSS -->
<link rel="stylesheet" href="css/pushy.css">
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- <script src="./js/table.js"></script> -->