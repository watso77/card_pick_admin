<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>
App Version 리스트
</title>

<?php 
include 'meta.php';
?>
</head>

<body>

<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'head_navi.php';
include "./config/define.php";
include "./config/db.php";
include './lib/util.php';
?>

<!-- Your Content -->
<div id="container">
	<h2>App Version 리스트
	</h2>

	<?php 
		$listHelper= new ListHelper;
		
		$urlParams = "";
		$totalCount = $listHelper->getQueryTotalCount($mysqli, "SELECT * FROM app");
		$perpage = SAFE_CALL_LIST_PER_PAGE; // This you can define yourself
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$startIndex = ((int)$page * (int)$perpage) - $perpage;
		$limitStr = "LIMIT " . $startIndex . "," . $perpage;
		
		//where 절 추가 요망
		$sql = "SELECT
				id
				, version_name
				, version_code
				, fingerprint
				, os
				FROM app
				ORDER BY id ASC
				$limitStr";
				
		$stmt = $listHelper->getStmt($mysqli, $sql);
		$stmt->bind_result(
				$id
				, $version_name
				, $version_code
				, $fingerprint
				, $os
				);
		
		if ($totalCount == 0)
			echo"등록 내용이 없습니다.<div class='CSSTableGenerator' style='display:none' >";
		else
			echo"<div class='CSSTableGenerator' >";
		
		echo 
		"<table border='1' width='100%' >
			<tr>
				<td>번호</td>
				<td>버전네임</td>
				<td>버전코드</td>		
				<td>key</td>
				<td>OS</td>		
			</tr>
		";
		
		$num  =  $totalCount - ($page * $perpage) + $perpage;
		while ($stmt->fetch()) {
			$key = StringHelper::toAsterisk($fingerprint);
			echo
			"<tr>
				<td>$num</td>
				<td>$version_name</td>
				<td>$version_code</td>
				<td>$key</td>
				<td>$os</td>
			</tr>
			";
			$num--;
		}
		
		$stmt->close();
		$mysqli->close();
		
		echo 
		"<table>";
		
		(new Paging)->printAppVersionPaging($stmt, $page, $totalCount, $perpage, $urlParams);
		
	?>
	</div>

</div>

<?php 
include 'footer.php';
?>


</body>
</html>