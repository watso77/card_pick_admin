<header class="site-header push">
	<a href="#" style="float:left" class="menu-btn">
		<img src="./img/menu.png" height="17px" valign="middle">&nbsp; MENU
	</a>
	Card Pick Manager 1.0
</header>

<nav class="pushy pushy-left">
<ul>
	<li class="pushy-submenu">
		<a href="#">안심콜</a>
        <ul>
        	<li class="pushy-link"><a href="./safe_call_list.php">안심콜 리스트</a></li>
            <li class="pushy-link"><a href="./safe_call_grant.php">안심콜 부여</a></li>
            <li class="pushy-link"><a href="#">예약 전화</a></li>
            <li class="pushy-link"><a href="./history_safe_call_list.php">안심콜 전화내역</a></li>
		</ul>
	</li>
	<li class="pushy-submenu">
    	<a href="#">회원</a>
		<ul>
			<li class="pushy-link"><a href="./member_list.php">회원리스트</a></li>
			<li class="pushy-link"><a href="#">회원등록</a></li>
		</ul>
		</li>
		<li class="pushy-submenu">
			<a href="#">설계사</a>
				<ul>
					<li class="pushy-link"><a href="./planner_list.php">설계사 리스트</a></li>
					<li class="pushy-link"><a href="./planner_insert.php">설계사등록</a></li>
					<li class="pushy-link"><a href="#">결제리스트</a></li>
				</ul>
		</li>
		
		<li class="pushy-submenu">
			<a href="#">어플리케이션</a>
			<ul>
				<li class="pushy-link"><a href="app_list.php">App version 리스트</a></li>
				<li class="pushy-link"><a href="#">App version 등록</a></li>
			</ul>
		</li>
		
		
		<li class="pushy-submenu">
			<a href="#">어플리케이션 / 컨텐츠</a>
			<ul>
				<li class="pushy-link"><a href="#">컨텐츠등록</a></li>
				<li class="pushy-link"><a href="#">공지사항 리스트</a></li>
				<li class="pushy-link"><a href="#">공지사항 등록</a></li>
			</ul>
		</li>
		
		<li class="pushy-submenu">
			<a href="#">어플리케이션 / GCM</a>
			<ul>
				<li class="pushy-link"><a href="#">GCM / type-A</a></li>
				<li class="pushy-link"><a href="#">GCM / type-B</a></li>
			</ul>
		</li>
		
		<li class="pushy-submenu">
			<a href="#">매니저설정</a>
			<ul>
				<li class="pushy-link"><a href="#">운영자등록</a></li>
				<li class="pushy-link"><a href="#">시작페이지변경</a></li>
			</ul>
		</li>
</ul>

</nav>

<!-- Site Overlay -->
<div class="site-overlay"></div>
<!-- Site Overlay -->
<div class="site-overlay"></div>
