<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>
설계사 등록
</title>

<?php 
include "./meta.php";
?>

<script src="./js/check.js"></script>
<script>

$(document).ready(function(){
	
	//아이디 유효성 검사
	$("#dupCheck").click(function(){
		if( idcheck($("input[name=register_id]").val() ))
		{
			$.ajax({
					type : 'get',
					url : './pre/card.php?code=' + code + types,
					dataType : 'html',
					success : function(data) {

						//유효성 합격
						$("input[name=is_id_check]").val("1");
						//$("#card_div").html(data);
					}
				});
		}
	});

	//submit
	$("#submit").click(function(){
		
		if($("input[name=name]").val() == ""
			|| $("input[name=email]").val() == ""
			|| $("input[name=tel]").val() == ""
			|| $("input[name=planner_num]").val() == ""
			|| $("input[name=major_card_company]").val() == ""						
			|| $("input[name=issued_zone]").val() == ""						
			|| $("input[name=gender]").val() == ""						
			|| $("input[name=password]").val() == ""						
		)
		{
			alert("빠진 항목이 있습니다.");
			return;			
		}

		if($("input[name=is_id_check]").val() == "0"){
			alert("아이디 중복 체크 해 주세요.");
			return;			
		}

		if($("input[name=password]").val() != $("input[name=password1]").val())
		{
			alert("패스워드를 확인해 주세요.");
			return;			
		}

		$('form').submit();		
		
	});
	
});

</script>


</head>

<body>
<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'head_navi.php';
include "./config/define.php";
include "./config/db.php";
include './lib/util.php';
?>

<div id="container">
	<h2>설계사 등록</h2>
	<ul style="margin-left:25px">
		<li><font color="red">*</font> : 반드시 입력</li>
	</ul>
	
	<div class='CSSTableGenerator'>
		
		<form name="form0" action="./planner_insert_post.php" method="GET"> 
			
				<table border="1">
					<tr>
						<td>구분</td><td>	내용</td>
						
					</tr>
					<tr>
						<td><font color="red">*</font>이름</td>
						<td><input name="name" type="text" size="50"></td>
					</tr>
					<input type="hidden" name="is_id_check" value="0">					
					<tr>
						<td><font color="red">*</font>아이디</td>
						<td>
							<input name="register_id" type="text" size="50">
							<div id="dupCheck" class="myButton" style="height:10px;margin-left:20px;font-size:8pt" >
								중복검사
							</div>
							(4-12자리), 한글과 특수문자 제외 
						</td>
					</tr>
					<tr>
						<td><font color="red">*</font>패스워드</td>
						<td><input name="password" type="text" size="50"></td>
					</tr>
					<tr>
						<td><font color="red">*</font>패스워드 확인</td>
						<td><input name="password1" type="password" size="50"></td>
					</tr>
					
					<tr>
						<td><font color="red">*</font>이메일</td>
						<td><input name="email" type="text" size="50"></td>
					</tr>
					<tr>
						<td><font color="red">*</font>성별</td>
						<td>
							남 <input name="gender" type="radio" value="m" checked>&nbsp;
							여 <input name="gender" type="radio" value="f" >
						</td>
					</tr>
					<tr>
						<td><font color="red">*</font>전화</td>
						<td><input name="tel" type="text" size="50"></td>
					</tr>
					<tr>
						<td><font color="red">*</font>주소</td>
						<td><input name="addr" type="text" size="50"></td>
					</tr>
					<tr>
						<td><font color="red">*</font>주카드사</td>
						<td><input name="major_card_company" type="text" size="50"></td>
					</tr>
					<tr>
						<td><font color="red">*</font>카드픽 설계사 인증</td>
						<td>
							미인증 <input name="status" type="radio" value="0" checked>&nbsp;
							인증 <input name="status" type="radio" value="1" >
						</td>
					</tr>
					
					<tr>
						<td><font color="red">*</font>설계사번호</td>
						<td><input name="planner_num" type="text" size="50"></td>
					</tr>
					<tr>
						<td><font color="red">*</font>발급지역</td>
						<td><input name="issued_zone"  type="text" size="50">
							<div id="selectZone" class="myButton" style="height:10px;margin-left:20px;font-size:8pt" >
								지역선택
							</div>
						</td>
					</tr>
				</table>
				
				<p style="margin:10px">
					
				<div id="submit" class="myButton" >등 록</div>
				
			
			</form>
	</div>
	
</div>

<?php 
include 'footer.php';
?>


</body>

</html>