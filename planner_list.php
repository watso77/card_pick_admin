<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>
설꼐사 리스트
</title>

<?php 
include 'meta.php';
?>
</head>

<body>

<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'head_navi.php';
include "./config/define.php";
include "./config/db.php";
include './lib/util.php';
?>

<!-- Your Content -->
<div id="container">
	<h2>설계사 리스트</h2>

	<?php 
		$listHelper= new ListHelper;
		
		$urlParams = "";
		$totalCount = $listHelper->getQueryTotalCount($mysqli, "SELECT * FROM planner");
		$perpage = SAFE_CALL_LIST_PER_PAGE; // This you can define yourself
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$startIndex = ((int)$page * (int)$perpage) - $perpage;
		$limitStr = "LIMIT " . $startIndex . "," . $perpage;
		
		//where 절 추가 요망
		$sql = "SELECT
				id
				, name
				, register_id
				, email
				, tel
				, planner_num
				, gender 
				, addr
				, major_card_company
				, date_insert
				, date_update
				, issued_zone
				, counsel_tile
				, status
				FROM planner
				ORDER BY id ASC
				$limitStr";
				
		$stmt = $listHelper->getStmt($mysqli, $sql);
		$stmt->bind_result(
				$id
				, $name
				, $register_id
				, $email
				, $tel
				, $planner_num
				, $gender
				, $addr
				, $major_card_company
				, $date_insert
				, $date_update
				, $issued_zone
				, $counsel_tile
				, $status
				);
		
		if ($totalCount == 0)
			echo"등록 내용이 없습니다.<div class='CSSTableGenerator' style='display:none' >";
		else
			echo"<div class='CSSTableGenerator' >";
		
		echo 
		"<table border='1' width='100%' >
			<tr>
				<td>이름</td>
				<td>아이디</td>		
				<td>이메일</td>
				<td>전화</td>		
				<td>설계사번호</td>
				<td>성별</td>
				<td>주소</td>
				<td>주카드사</td>
				<td>등록일</td>
				<td>업데이트</td>		
				<td>발급지역</td>		
				<td>등록시문의내용</td>		
				<td>인증상태</td>		
			</tr>
		";
		
		$num  =  $totalCount - ($page * $perpage) + $perpage;
		while ($stmt->fetch()) {
			($status == 0) ? $mStatus = "미인증" : $mStatus = "인증";
			echo
			"<tr>
				<td>$name</td>
				<td>$register_id</td>
				<td>$email</td>
				<td>$tel</td>
				<td>$planner_num</td>
				<td>$gender</td>
				<td>$addr</td>
				<td>$major_card_company</td>
				<td>$date_insert</td>
				<td>$date_update</td>
				<td>$issued_zone</td>
				<td>$counsel_tile</td>
				<td>$mStatus</td>		
			</tr>
			";
			$num--;
		}
		
		$stmt->close();
		$mysqli->close();
		
		echo 
		"<table>";
		
		(new Paging)->printMemberPaging($stmt, $page, $totalCount, $perpage, $urlParams);
		
	?>
	</div>

</div>

<?php 
include 'footer.php';
?>


</body>
</html>