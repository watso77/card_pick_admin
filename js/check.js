function idcheck(value) {
	var result;
	
	/**********************************************************************************
	 *아이디 체크하는 자바 스크립트
	1.아이디값이 있는지 체크한다.
	2.자리수를 체크한다.(4-12자리)
	3.한글과 특수문자를 쓰지 못하게 한다.
	 ***********************************************************************************/
	var ch;
	//">~!@#$%^&*()-_=+|\\{}[];:"\'<>,.?\/';//특수 문자를 미리 등록시킨다.
	var special = ' ,.?\/';
	if (value == "") {//아이디가 없다면 alert창 띄운다.
		alert("아이디를 입력하세요");
		//login_form.id.focus();
		return false;
	}
	if (value.length < 4 || value.length > 12) {
		//아이디가 4자 이하이거나 12자 이상이면 alert창 띄운다.
		alert("아이디를 4-12로 해주세요");
		//login_form.id.value = "";
		//login_form.id.focus();
		return false;
	}

	//한글과 특수문자를 쓰지 못하게 하는 구문
	for (var i = 0; i < value.length; i++) {
		ch = value.charAt(i);//아이디 입력폼에 값을 하나씩 가져온다.

		if (((ch >= "ㅏ") && (ch <= "히")) || ((ch >= "ㄱ") && (ch <= "ㅎ"))) {
			//모든 한글을 확인하는 구문. 한글이 아니라면 alert창 띄우기.
			alert("한글은 안됩니다.");
			//login_form.id.focus();
			return false;
		}
		
		//특수문자를 확인하는 구문.미리 등록된 특수문자를 하나씩 가져온다.
		for (var j = 0; j < special.length; j++) {
			if (ch == special.charAt(j)) {
				//아이디 입력폼의 문자를 하나씩 가져와서 미리 등록된 특수문자를 비교하여 같다면 특수문자이다. 
				alert("특수 문자는 안됩니다.");
				//login_form.id.value = "";
				//login_form.id.focus();
				return false;
			}
		}
	}
	
	alert(value + "은 사용가능한 아이디 입니다.");//모든 조건이 만족하다면 입력가능한 아이디.
	return true;
}