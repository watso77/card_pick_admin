<?php 
header('Content-Type: text/html; charset=utf-8');
include "./config/define.php";
include "./config/db.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (trim($_GET['safe_tel_range0']) == "" || strlen(trim($_GET['safe_tel_range0'])) != 4 
	||	trim($_GET['safe_tel_range1']) == "" || strlen(trim($_GET['safe_tel_range1'])) != 4
	||	trim($_GET['safe_tel0']) == "" || strlen(trim($_GET['safe_tel0'])) != 4
	||	trim($_GET['safe_tel1']) == "" || strlen(trim($_GET['safe_tel1'])) != 4
) {
	echo "
		<script>
			alert('입력값이 잘못 되었습니다.');
			history.back();
		</script>
		";
	exit;
}
	
$stmt_all_del = $mysqli->prepare("delete from call_grant");
$stmt_all_del->execute();
$stmt_all_del->close();


for ($i = trim($_GET['safe_tel_range0']) + 0 ; $i <= trim($_GET['safe_tel_range1']) + 0 ; $i++){
	$tel = trim($_GET['safe_tel0']) . trim($_GET['safe_tel1']) . $i;
	$sql = "INSERT INTO call_grant(VirtualNumber, consumer_company,type, date_input) VALUES (?, ?, ?, now())";
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param('sss', $VirtualNumber, $consumer_company, $type);
	$VirtualNumber = $tel;
	$consumer_company = CONSUMER_COMPANY;
	$type = SAFE_CALL_TYPE;
	$stmt->execute();
	$stmt->close();
}

$mysqli->close();

echo "
		<script>
			alert('등록 완료');
			history.back();
		</script>
		";

?>



